from source.parser import Parser
from source.validator import Validator


if __name__ == "__main__":
    parsed = Parser().parse()
    validator = Validator(parsed=parsed)
    validator.validate()
