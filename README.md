# Invoice Validator

## Purpose
This code was created to make my life easier. I worked for two companies for a while thus I needed two invoices each 
month. I used two different account numbers for each invoice. I find it quite easy to make a mistake and type the wrong
account number (in fact made this mistake once). I decided to create a tool which can easily check whether an invoice
has the proper matching between the account number and the company. In fact the code is capable of checking as many 
types of invoice as you wish.

Of course, it was a great opportunity to develop some nice programming skills and to start with my own portfolio.

## Installation
There are no special needs or requirements to install this tool.

To use the code provided here you just need to:
1. clone this repository using `git clone`,
2. install all the requirements using `pip install -r requirements.txt` from the parent directory.

Please note that I used Python 3.10.6 during the development however all versions 3.6+ will be suitable.

## Usage
Suggested way of storing your invoices.
```
invoices
|
|__ Company1
|    |
|    |__ February 2022
|           |
|           |__ <name of the invoice>.pdf
|
|__ Company2
|    |
|    |__ February 2022
|           |
|           |__ <name of the invoice>.pdf
```

### Configuration of your tool
To use this code you need to work on the `source/config.py`. There are several variables which should be filled:
1. `INVOICED_COMPANIES`: stores the names of the companies which are invoiced. For example if you work for company named "A" and "B" you need to insert the names inside as strings.
2. `FILES_TO_CHECK`: the names of your invoices. My invoices are always named in the same way, i.e. "Invoice -- Dawid.pdf" and that would be put inside this variable in my case,
3. `MAIN_DIRECTORY`: the name of the directory where you store your invoices. I recommend to store them in the following way `.../Invoices/Company1/`, `Invoices/Company2/`, etc. The main directory would be `../Invoices` in this case.
4. `PROPER_VALUES`: that's a dictionary containing the company name and the proper account number, companies are keys and account numbers are values, both should be saved as strings,
5. `PATTERNS`: another dictionary containing the regex pattern to find the buyer inside your invoice. En example could be `r'Buyer (.+)\n'`. You need to check that inside your invoice. The default pattern for account number is a standard Polish number. You might want to change this too.

You might also want to change some values in `ENG2POL_MONTHS`. I store use Polish names of moths to store my invoices
(see the diagram above) however the command uses English words only, that's why I need a mapping.

### Testing the configuration
When you complete filling the config file you may want to run the following command from the parent directory.
```
pytest tests/
```
The tests are able to check whether your config file is filled properly.

### Using the tool
Now it's time to validate whether your invoices are completed correctly. You just need to type in your terminal window
the following command.
```
python main.py
```

### Reading the output
You will see some output in the terminal window, there are two types of messages:
1. `[ INFO ] <path to your invoice> valid.` -- means that everything is alright with the account number,
2. `[ WARNING ] <path to your invoice> invalid` -- means that the account number is wrong.

You will also be able to see the `.log` file in the `logs` directory. The logs provide additional information if 
there's a warning -- the number of the account which is in the invoice and the proper number.

### Additional parameters
You can also run the tool with some additional flags:
1. `-m, --month` specifies the month, only invoices from this month will be checked,
2. `-y, --year` specifies the year, only invoices from this year will be checked,
3. `--print, --no-print` specifies whether the results should be also printed in the terminal window.

For more details please run `python main.py --help` in the parent directory.

### Examples
To check all the invoices you can use the following command.
```
python main.py
```

To check just the invoices from April you can use the following command.
```
python main.py --month april
```

To check all the invoices from 2022 you can use the following command.
```
python main.py --year 2022
```

## Support
If you have any questions about this code feel free to email me @dawidhanrahan@gmail.com.

## License
[MIT](https://gitlab.com/Hendrra/invoice-validador/-/blob/main/LICENSE)

## Project status
The project is completed and no further work is planned.