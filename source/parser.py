import argparse
import datetime

from source.cleaners import CleanerFactory
from source.config import ALL_MONTHS


class Parser:
    """
    Parses the arguments provided by the user in the terminal. Cleans them properly.

    The Parser contains the following public methods:
        1) parse -- returns the cleaned, parsed arguments.
    """

    def __init__(self, cleaner_factory=CleanerFactory):
        """Initialises the Parses instance.

        Args:
            cleaner_factory (CleanerFactory): the factory used to provide the Parser
            with the proper Cleaning Class.
        """
        self._args = None
        self._data = {}
        self._cleaner_factory = cleaner_factory

    def parse(self):
        """Parses and cleans the arguments. Returns them as a dictionary.

        Returns:
            dict: a dictionary with parsed arguments.
        """
        self._parse_arguments()
        attributes = [attr for attr in dir(self.args) if not attr.startswith('_')]
        for attribute in attributes:
            self._clean_arguments(attribute=attribute)
        return self._data

    def _parse_arguments(self):
        """Creates the parser and enables the class to parse arguments from the terminal.
        """
        parser = argparse.ArgumentParser(description='Argument parser for the Invoice Validator.')

        help_message_for_month = 'Argument which stores the month for which the invoices should be validated. ' \
                                 'If one want to validate all the available invoices then should use `All Months`. ' \
                                 'It is also the default parameter.'
        parser.add_argument(
            '-m', '--month',
            help=help_message_for_month,
            type=str,
            required=False,
            default=ALL_MONTHS,
        )

        help_message_for_year = 'Argument which stores the month for which the invoices should be validated. ' \
                                'If one want to validate all the available invoices then should use `All Years`. ' \
                                'By default it is equal to the current year.'
        parser.add_argument(
            '-y', '--year',
            help=help_message_for_year,
            type=str,
            required=False,
            default=datetime.date.today().year,
        )

        help_message_for_print = 'Argument which indicates whether the program should print the results. ' \
                                 'By default it displays the result.'
        parser.add_argument(
            '--print',
            help=help_message_for_print,
            action=argparse.BooleanOptionalAction,
            required=False,
            default=True,
        )

        self.args = parser.parse_args()

    def _clean_arguments(self, attribute):
        """Gets the proper cleaning class and cleans the chosen attribute.

        Args:
            attribute (str): the name of the parsed attribute.
        """
        cleaning_class = self._cleaner_factory.get_cleaning_class(attribute=attribute)
        cleaned_attribute = cleaning_class(args=self.args).clean()
        self._data[attribute] = cleaned_attribute
