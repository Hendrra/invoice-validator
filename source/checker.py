import logging
import os

from source.config import MAIN_DIRECTORY
from source.config import LOG_DIRECTORY
from source.config import PROPER_VALUES


logging.basicConfig(
    filename=LOG_DIRECTORY,
    level=logging.INFO,
    format='%(asctime)s:%(levelname)s:%(message)s',
)


class Checker:
    """
    Checks whether an invoice contains the proper account.

    The Checker contains the following public methods:
        1) check -- verifies whether the invoice is valid.
    """

    @classmethod
    def check(cls, path, summary, print_info):
        """The main function used for validation. It verifies whether an invoice
        contains the proper account number and saves the result to a .log file.
        It is also capable of printing the results in the terminal window.

        If an invoice contains proper data this function says that this invoice is valid.
        Otherwise, it logs a warning message saying that the invoice is invalid and suggests
        which account number should be the proper one for this invoice.

        Messages displayed in the terminal window are a bit more laconic; they say whether
        an invoice is valid or not.

        Args:
            path (str): full path to an invoice which is being checked,
            summary (dict): dictionary containing two keys: `buyer` (the name of the payer)
            and `account` (connected to the collector),
            print_info (bool): indicates whether the results should be printed in the terminal
            window.
        """
        relative_path = os.path.relpath(path, MAIN_DIRECTORY)
        name, account = summary.values()
        if PROPER_VALUES[name] == account:
            if print_info:
                print(f'[ INFO ]: {relative_path} valid.')
            logging.info(f'{relative_path} VALID.')
        else:
            if print_info:
                print(f'[ WARNING ]: {relative_path} invalid!')
            logging.warning(f'{relative_path} INVALID. Should be {PROPER_VALUES[name]} is {account}')
