import re

from source.config import PATTERNS


class Interpreter:
    """
    Interprets the content of extracted files. Searches for patterns
    such as buyer or account number.

    The Interpreter has the following public methods:
        1) interpret -- iterates over patterns from the config file and
        looks for data.
    """

    @classmethod
    def interpret(cls, content):
        """Creates a dict. Uses the patterns and their names provided inside
        the config file.

        Args:
            content (str): the content of a .pdf file (for more details see Extractor).

        Returns:
            dict: dictionary containing the interpreted values from the patters.
        """
        summary = {}
        for name, pattern in PATTERNS.items():
            m = re.search(pattern, content)
            try:
                value = m.group(1)
            except AttributeError:
                value = None
            summary[name] = value
        return summary
