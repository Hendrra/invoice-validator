import abc
import functools
import importlib

from source.config import ALL_MONTHS
from source.config import ENG2POL_MONTHS


class CleanerFactory:
    """
    Factory which is capable of choosing the proper cleaning class for
    a given attribute.

    This factory contains the following public methods:
        1) get_cleaning_class -- returns the proper cleaning class.
    """

    @classmethod
    def get_cleaning_class(cls, attribute):
        """Chooses the proper cleaning class based on the attribute which
        comes from parsed data (i.e. from the user).

        Args:
            attribute (str): the attribute which comes from the argparser.

        Note! In fact all given attributes are supported, however by default
        an attribute will remain unchanged. If you want to format/clean an
        attribute properly see the docstring in `CleanerForAttributeBase`.

        Returns:
            class: proper cleaning class; child of CleanerForAttributeBase.
        """
        module = importlib.import_module(__name__)
        try:
            class_ = getattr(module, 'CleanerFor' + attribute.capitalize())
        except AttributeError:
            class_ = DummyCleaner
        class_ = functools.partial(class_, attribute=attribute)
        return class_


class CleanerForAttributeBase(metaclass=abc.ABCMeta):
    """
    Base, abstract class which is used to create any cleaning class
    for a certain attribute.

    If you need to create a new cleaning class you should remember that:
        1) any child class should follow the naming convention: `CleanerFor<Attribute>`,
        2) you need to implement any logic inside the `clean` function,
        3) remember that the only public attribute of this class is `to_clean` and
        that attribute stores the data which was parsed by the user (thus any formatting
        should be done using this variable).

    All the child classes will have the following public methods:
        1) `clean` -- does the cleaning, i.e. formats the user input properly.

    All the child classes will have the following public attributes:
        1) `to_clean` -- stores anything the user provided.
    """

    def __init__(self, attribute, args):
        """Initialises the Cleaner class.

        Args:
            attribute (str): the name of the attribute which should be formatted/cleaned,
            args (argparse.Namespace): the namespace containing the input from the terminal
            provided by the user.
        """
        self.to_clean = getattr(args, attribute)

    @abc.abstractmethod
    def clean(self):
        pass


class DummyCleaner(CleanerForAttributeBase):
    """
    Cleaner which does nothing, i.e. remains the user's input unchanged.
    """

    def clean(self):
        return self.to_clean


class CleanerForMonth(CleanerForAttributeBase):
    """
    Cleaner for `month`. Translates the name of the month into Polish or
    returns the information that all months should be considered.
    """

    def clean(self):
        self.to_clean = self.to_clean.capitalize()
        try:
            self.to_clean = ENG2POL_MONTHS[self.to_clean]
        except KeyError:
            self.to_clean = ALL_MONTHS
        return self.to_clean


class CleanerForYear(CleanerForAttributeBase):
    """
    Cleaner for `year`. Makes a string from an integer.
    """

    def clean(self):
        return str(self.to_clean)
