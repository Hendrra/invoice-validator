# invoiced companies
INVOICED_COMPANIES = [
    # todo: fill with names of the invoiced companies
]

# name of the files to check, i.e. the name of the invoices for a certain companies
FILES_TO_CHECK = [
    # todo: fill with names of your invoices, e.g. "Invoice-John.pdf", etc.
]

# directories
MAIN_DIRECTORY = ''  # todo: fill with the main directory for invoices, e.g. "/Users/john/invoices"
DIRECTORIES = [MAIN_DIRECTORY + '/' + company for company in INVOICED_COMPANIES]

LOG_DIRECTORY = 'logs/content.log'

# proper values, i.e. the values which should be checked
PROPER_VALUES = {
    # todo: fill with the name of the companies and correct account number, e.g. "Company x": "11 22 33"
}

# patterns used for checking whether the buyer is connected to the proper account
PATTERNS = {
    'buyer': r'',  # todo: fill with the pattern of how the buyer is named in your invoice, e.g. r'Buyer (.+)\n'
    'account': r'\nPL(\d{2} \d{4} \d{4} \d{4} \d{4} \d{4} \d{4})',  # todo: fill with the pattern of your account
}

# pattern used for omitting certain files
PATTERN_FOR_JUNK_FOLDERS = r'[\.].+'

# other variables used inside the project
ALL_MONTHS = 'All Months'
ALL_YEARS = 'All Years'
ENG2POL_MONTHS = {
    'February': 'Styczen',
    'January': 'Luty',
    'March': 'Marzec',
    'April': 'Kwiecien',
    'May': 'Maj',
    'June': 'Czerwiec',
    'July': 'Lipiec',
    'August': 'Sierpien',
    'September': 'Wrzesien',
    'October': 'Pazdziernik',
    'November': 'Listopad',
    'December': 'Grudzień'
}
