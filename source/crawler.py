import os
import re

from source.config import ALL_MONTHS
from source.config import ALL_YEARS
from source.config import DIRECTORIES
from source.config import FILES_TO_CHECK
from source.config import PATTERN_FOR_JUNK_FOLDERS


class Crawler:
    """
    Crawls through all subdirectories specified in the config file. Checks whether a file
    is a proper one (i.e. a .pdf file which is also an invoice).

    The Crawler contains the following public methods:
        1) crawl -- crawls through the subdirectories and grab the paths of the proper files.
    """

    @classmethod
    def crawl(cls, month, year):
        """Crawls through the subdirectories specified in the config file in order to
        create a list with paths to the proper files, i.e. files which are invoices.
        Additionally, the files can be filtered by a month or by a year.

        Args:
            month (str): name of the month, used to filter the list of invoices, more
            precisely only invoices in the specified month will be returned,
            year (str): year, used to filter the list of invoices, more
            precisely only invoices in the specified month will be returned.

        Example. Imagine there are two invoices in the subdirectories. One from April 2020 and
        the second from April 2021.
        If month = April and year is not specified (by default all) thus two paths will be returned.
        However, if month = April and year = 2021 just one invoice (the path) will be returned.

        Returns:
            list: the paths of the proper files.
        """
        files = []
        for directory in DIRECTORIES:
            for folder in os.listdir(directory):
                if re.match(PATTERN_FOR_JUNK_FOLDERS, folder):
                    continue
                path = os.path.join(directory, folder)
                for file in os.listdir(path):
                    if cls._is_proper_file(path=path, file=file, month=month, year=year):
                        files.append(os.path.join(path, file))
        return files

    @classmethod
    def _is_proper_file(cls, path, file, month, year):
        """Checks whether a file is proper. There are three basic filtering criteria:
        1) the proper name; the function checks whether the name is inside FILES_TO_CHECK
        which is a variable which has to be filled inside the config file,
        2) month; the files can be also filtered by the month,
        3) year; the files can be also filtered by the year.

        Args:
            path (str): path to the directory which stores the checked file,
            file (str): the name of the file wih extension,
            month (str): name of the month (or ALL_MONTHS),
            year (str): the year (of ALL_YEARS).

        Returns:
            boolean: True if the file is proper, False otherwise.
        """
        proper_name = file in FILES_TO_CHECK

        if year == ALL_YEARS:
            if month == ALL_MONTHS:
                return proper_name
            proper_month = month in path
            return proper_name and proper_month
        else:
            proper_year = year in path
            if month == ALL_MONTHS:
                return proper_name and proper_year
            proper_month = month in path
            return proper_name and proper_month and proper_year
