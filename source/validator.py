from source.checker import Checker
from source.crawler import Crawler
from source.extractor import Extractor
from source.interpreter import Interpreter


class Validator:
    """
    Validator is a facade which gathers all the functionalities of the other tools
    used to verify an invoice.

    The Validator class has the following public methods:
        1) validate -- does the validation of all invoices inside given directories.
    """

    def __init__(self, parsed, crawler=Crawler, extractor=Extractor, interpreter=Interpreter, checker=Checker):
        """Initialises the Validator.

        Args:
             parsed (dict): dictionary containing parsed and cleaned arguments from the user,
             crawler (Crawler): the Crawler class, used for crawling through the files,
             extractor (Extractor): the Extractor class, used for extracting the data from a single .pdf file,
             interpreter (Interpreter): the Interpreter class, used for extracting information from .pdf file,
             checker (Checker): the Checker class, used for checking whether the invoice has correct data inside.
        """
        self._month = parsed['month']
        self._print = parsed['print']
        self._year = parsed['year']

        self._crawler = crawler
        self._extractor = extractor
        self._interpreter = interpreter
        self._checker = checker

    def validate(self):
        """Validates the invoices inside the given directories. Prints information in the terminal window
        (if wanted) and stores them inside a .log file.
        """
        paths = self._crawler.crawl(month=self._month, year=self._year)
        for path in paths:
            content = self._extractor.extract(path=path)
            summary = self._interpreter.interpret(content=content)
            self._checker.check(path=path, summary=summary, print_info=self._print)
