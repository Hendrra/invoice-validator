import os.path

import pytest

from source.config import DIRECTORIES
from source.config import FILES_TO_CHECK
from source.config import MAIN_DIRECTORY
from source.config import INVOICED_COMPANIES
from source.config import PATTERNS
from source.config import PROPER_VALUES


# the tests below check whether the config file has been filled properly


@pytest.mark.parametrize(
    'variable', [
        INVOICED_COMPANIES,
        FILES_TO_CHECK,
    ]
)
def test_filled_array_variables(variable):
    assert len(variable) > 0


@pytest.mark.parametrize(
    'variable', [
        INVOICED_COMPANIES,
        FILES_TO_CHECK,
    ]
)
def test_filled_array_variables_with_data(variable):
    for item in variable:
        assert len(item) > 0


def test_filled_main_directory():
    assert len(MAIN_DIRECTORY) > 0


def test_directories_exists():
    for directory in DIRECTORIES:
        os.path.exists(directory)


def test_filled_proper_values():
    assert len(PROPER_VALUES.keys()) > 0


def test_filled_proper_values_with_data():
    for value in PROPER_VALUES.values():
        assert len(value) > 0


def test_filled_patterns():
    assert len(PATTERNS['buyer']) > 0
    assert len(PATTERNS['account']) > 0
